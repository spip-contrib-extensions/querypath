<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '9e5f8b10cd1e7816b42a606c779d56dd82c91d29',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '9e5f8b10cd1e7816b42a606c779d56dd82c91d29',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'arthurkushman/query-path' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '3.1.4',
            ),
        ),
        'gravitypdf/querypath' => array(
            'pretty_version' => '4.1.0',
            'version' => '4.1.0.0',
            'reference' => '32b08b5b5295fa3b84baeb91685c66ca86b1c591',
            'type' => 'library',
            'install_path' => __DIR__ . '/../gravitypdf/querypath',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'masterminds/html5' => array(
            'pretty_version' => '2.9.0',
            'version' => '2.9.0.0',
            'reference' => 'f5ac2c0b0a2eefca70b2ce32a5809992227e75a6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../masterminds/html5',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'querypath/querypath' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '3.0.5',
            ),
        ),
    ),
);
