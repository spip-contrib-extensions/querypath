<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'QueryPath\\' => array($vendorDir . '/gravitypdf/querypath/src'),
    'QueryPathTests\\' => array($vendorDir . '/gravitypdf/querypath/tests/QueryPath'),
    'Masterminds\\' => array($vendorDir . '/masterminds/html5/src'),
);
